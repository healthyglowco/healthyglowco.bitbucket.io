var exoViewShown = false;

$(document).ready(function() {

  checkVideoStatus();

  $('.table-view-cell.ex-1').click(function() {
    toggleExoView('exoOne');
  });

  $('.table-view-cell.ex-2').click(function() {
    toggleExoView('exoTwo');
  });

  $('.icon-close').click(function() {
    toggleExoView();
  });

  $('.icon-play').click(function() {
    startVideo();
  });

  $('.exos-list-item').click(function() {
    $('.exos-list').css('display', 'block');
    $('.high-fives').css('display', 'none');
  });

  $('#startButton').click(function() {
    $('#canvas').css('display', 'block');
  });

  $('.high-fives-item').click(function() {
    $('.exos-list').css('display', 'none');
    $('.high-fives').css('display', 'block');
  });

  $('.high-five-row').click(function() {
    alert('🎉 You gave Carrie a High Five! 🎉 👋👋')
  })

  $('.carousel').slick({
    dots: true,
    infinite: true,
    speed: 250,
    fade: true,
    cssEase: 'linear',
    autoplay: true,
    autoplaySpeed: 500
  });

});

function startVideo() {
  var video = $('.demo-video');
  video.css('display', 'block');
  video.get(0).play();
}

function checkVideoStatus() {
  var video = $('.demo-video');
  if (video.get(0).paused) {
    video.css('display', 'none');
  } else {
    video.css('display', 'block');
  }
}

function toggleExoView(exoName) {
  var exoView = $('.exo-view.exo-one');
  if (exoName == 'exoTwo') {
    exoView = $('.exo-view.exo-two');
  }

  if (!exoViewShown) {
    setTimeout(function() {
      exoView.css('display', 'block');
      $('.table-view.main-tbv').css('display', 'none');
      exoViewShown = true;
    }, 100);
  } else {
    setTimeout(function() {
      $('.exo-view.exo-one').css('display', 'none');
      $('.exo-view.exo-two').css('display', 'none');
      $('.table-view.main-tbv').css('display', 'block');
      exoViewShown = false;
    }, 100);
  }
}